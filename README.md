# Dockerun Host for Windows

**Start Dockerun generated containers with one click**

![](https://dockerun.francescosorge.com/dockerunhost/step-3-1.png)



## Use it

 - Download Dockerun Host for Windows from [here](https://dockerun.francescosorge.com/dockerunhost/DockerunHost.exe).
 - Open the file

Now you can use the "Launch containers now" button from the [Dockerun website](https://dockerun.francescosorge.com).



## Contribute

First of all, clone it using SSH

```bash
git clone git@gitlab.com:fsorge/dockerun-host.git
```

Open it with Visual Studio or your favorite C# .NET Framework WPF IDE



**That's all! Start doing your own experiments and improvements!**

**Note:** We suggest you to work in your own branch to avoid conflicts.



## Submit changes for review and approval

Before doing that, check that the app works correctly by trying to register with the OS (first run), if the Dockerun website can communicate correctly with the app and if Docker can correctly execute the command.

If the app compiles correctly and works, it's time to submit a merge request. Use GitLab to do that.

The owner of the repository will then check your changes and if everything is right, your branch will be merged with the official website.